**WARNING! Works on this project are in progress. Do not clone it, until this message is removed or until you're ready to finish it yourself. At this point it is pretty unusable at all!**

Simple PhoneGap 2.9.0 file and folder picker demo. Written in pure [PhoneGap File API](http://docs.phonegap.com/en/2.9.0/cordova_file_file.md.html), with no extra visual frameworks used (like jQuery Mobile).

It uses [Font Awesome](http://fontawesome.io/icons/) and [Zepto.js](http://zeptojs.com/).

Sources:

- [PhoneGap 2.9.0 File API](http://docs.phonegap.com/en/2.9.0/cordova_file_file.md.html),
- [File Chooser Dialog for PhoneGap Applications](http://ramkulkarni.com/blog/file-chooser-dialog-for-phonegap-application/).

Tested on four different devices and Android versions:

- Samsung Galaxy Nexus with Android 4.3 (previously Android 4.2.2),
- Sony Xperia E with Android 4.1.1,
- LG GT540 with Android 2.3.3 and CyanogenMod, 
- GSmart Rola G1317D with Android 2.2.2.

All seems to be fine.